# Docker

Documento de referência para apresentação da ferramenta.

## O que é Docker?
* Ferramenta que isola ambiente para execução de uma aplicação.
* Segrega a aplicação da infraestrutura.
* Alternativa (quase) perfeita às VMs
  - As imagens são baseadas em Linux  

## Arquitetura

![Arquitetura Docker](https://docs.docker.com/engine/images/architecture.svg)
https://docs.docker.com/engine/docker-overview/#docker-architecture

## Images
  - Descreve como deve ser montado um container

## Containers
  - Implementação de uma imagem
  - Empacotamento da aplicação com sua dependências.
  - Garante as mesmas condições em diferentes máquinas.

## Volumes
  - Mapeamento de diretórios no host

## Registry
  - Repositório de imagens
  - Por padrão, Docker aponta para o Docker Hub

## Pull/Push
  - Pull: obtém uma imagem de um registry
  - Push: envia uma imagem para um registry

## Docker no ME - Build
  - Comentar o Dockerfile
  - Apresentar em que contexto do ME o Dockerfile é usado

## Docker compose
  - Services
  - Networks
