class BasicSetup:
    DEBUG = False


class Development(BasicSetup):
    DEBUG = True


class Production(BasicSetup):
    pass


config = {
    'development': Development,
    'production': Production
}
