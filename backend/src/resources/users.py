from flask import Blueprint
from flask_restful import Api, Resource


class Users(Resource):
    def get(self):
        return {
            "name": "Testando"
        }

users_bp = Blueprint('users', __name__)
users_api = Api(users_bp)

users_api.add_resource(Users, '/users')
