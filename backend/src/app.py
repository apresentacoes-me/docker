import os
from flask import Flask
from .resources.users import users_bp
from .config import config


def create_app(env):
    app = Flask(__name__)
    app.config.from_object(config.get(os.getenv('FLASK_ENV')))

    url_prefix = '/api/v1'
    app.register_blueprint(users_bp, url_prefix=url_prefix)

    @app.route('/health')
    def index():
        return 'i am healthy'

    return app
