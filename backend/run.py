import os
from src.app import create_app


def run():
    environment = os.getenv('FLASK_ENV')
    app = create_app(environment)

    app.run()

if __name__ == '__main__':
    run()
