# API lista de usuários

Aplicação parte do exemplo prático do treinamento de Docker.

Para executar a aplicação, na pasta raiz, execute:
```sh
python run.py
```

## Endpoints

__GET__ _/api/v1/user
